# My Matrix Server

## Einrichten des Verzeichnisses

Als Grundlagerichte ich die Verzeichnise ein. Ich habe bereits die gesamte Konfiguration erstellt und veröffentlicht und kann daher einfach mein Repo klonen oder forken. Ich werde alle Dateien durchgehen, um zu erklären, was vor sich geht. Also einfach ausführen:

```
git clone https://gitlab.com/psycho0verload/my-matrix-server.git ./docker-server
```

Ich gehen nun mit `cd` in `docker-server` und ich sollte diese Baumstruktur vorfinden:

```
.
└── docker-server/
    ├── core/
    │   ├── traefik-data/
    │   │   ├── configurations/
    │   │   │   └── dynamic.yml
    │   │   ├── traefik.yml
    │   │   └── acme.json
    │   ├── portianer-data/
    │   └── docker-compose.yml
    └── [...]
```

## Einzelnen Projektteile

Das Projekt setzt sich aus mehreren Teilen zusammen. Jeder Teil erhhält seine eigene README.

1. [Traefik v 2.5 + Portainer als Core installieren](core/README.md)
2. [Matrix-Server](apps/comhub/README.md)
3. [Weitere Apps](apps/README.md)