# Traefik v 2.5 + Portainer als Core installieren

## Voraussetzung

- Ubuntu 20.04 - Server installiert
- Sichere Konfiguration des Servers
  - Public Key Authentifizierung
  - UFW
  - Deaktivierter Root-Login
- Docker und Docker-Compose installiert
- Benutzer ist der docker-Gruppe hinzugefügt
- apache2-utils installiert

## Core

Die Basis - der "Core" besteht aus einer "Portainer.io"-Instanz und einer "Traefik v2"-Instanz als Reverse Proxy.

### Aufbau meines Stacks 🚧

Von diesem Punkt an gehe ich davon aus, dass ich die [Voraussetzung](#voraussetzung) erfüllt habe.

#### DNS Records setzen

Also, als erstes müssen ich die entsprechenden Domains einrichten, damit ich auf meinen Portainer und das Traefik-Dashboard zugreifen kann. Ich wähle einfach eine der Domains, die ich im Laufe der Jahre angesammelt haben 😄.

Ich richte sie so ein, dass sie auf meinen Server zeigen:

```
traefik.meinedomain.com. IN A 123.321.216.34 
portainer.meinedomain.com. IN A 123.321.216.34
```
Beispiel-Inhalt `meinedomain.com` und `123.321.216.34` müssen auf meine eigenen Bedürfnisse angepasst werden!

Auf diese Weise wird mein Portainer & Traefik Dashboard unter den entsprechenden Subdomains verfügbar sein.

### Erklärung der Dateien ✅

#### 1. traefik.yml

Die erste Datei, die ich durchgehe, ist die Datei traefik.yml, wie im folgenden Code-Schnipsel zu sehen. Dies ist die statische Grundkonfiguration von Traefik.

Zuerst teilen ich Traefik mit, dass ich die Web-GUI möchte, indem ich `dashboard: true` setze

Danach definieren ich meine beiden Einstiegspunkte web (http) und websecure (https). Für meinnen sicheren https Endpunkt richte ich den certResolver ein, damit ich in den Genuss von automatischen Zertifikaten von Let's Encrypt komme! 😄 Als nächstes lade ich die entsprechende Middleware, damit mein gesamter Datenverkehr an https weitergeleitet wird.

Im Provider-Teil gebe ich an, dass diese Datei mittels bind mount an einen Docker-Container übergeben wird. Außerdem weise ich Traefik an, meine dynamische Konfiguration in configurations/dynamic.yml zu finden. Als letztes folgt die Konfiguration für meinen SSL-Zertifikatsresolver.

```
# traefik.yml
api:
  dashboard: true
entryPoints:
  web:
    address: :80
    http:
      redirections:
        entryPoint:
          to: websecure
  websecure:
    address: :443
    http:
      middlewares:
        - secureHeaders@file
      tls:
        certResolver: letsencrypt
providers:
  docker:
    endpoint: "unix:///var/run/docker.sock"
    exposedByDefault: false
  file:
    filename: /configurations/dynamic.yml
certificatesResolvers:
  letsencrypt:
    acme:
      email: mail@meinedomain.com
      storage: acme.json
      keyType: EC384
      httpChallenge:
        entryPoint: web
```

Beispiel-Inhalt `mail@meinedomain.com` muss auf meine eigenen Bedürfnisse angepasst werden!

#### 2. dynamic.yml

Diese Datei enthält meine Middlewares, um sicherzustellen, dass mein gesamter Datenverkehr völlig sicher ist und über TLS läuft. Außerdem lege ich hier die grundlegenden Berechtigungen für mein Traefik-Dashboard fest, da es standardmäßig für jeden zugänglich ist.

Die Datei ist vollständig dynamisch und kann ohne Neustart des Containers bearbeitet werden.

```
# dynamic.yml
http:
  middlewares:
    secureHeaders:
      headers:
        browserXssFilter: true
        contentTypeNosniff: true
        frameDeny: true
        sslRedirect: true
        #HSTS Configuration
        stsIncludeSubdomains: true
        stsPreload: true
        stsSeconds: 31536000
        forceSTSHeader: true
        customFrameOptionsValue: "SAMEORIGIN"
    user-auth:
      basicAuth:
        users:
          - "user:$apr1$MTqasdasrrkzT5ERGFqwH9f3uipxA1"
tls:
  options:
    default:
      cipherSuites:
        - TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
        - TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305
        - TLS_AES_128_GCM_SHA256
        - TLS_AES_256_GCM_SHA384
        - TLS_CHACHA20_POLY1305_SHA256
      curvePreferences:
        - CurveP521
        - CurveP384
      sniStrict: true
      minVersion: VersionTLS12
```

Beispiel-Inhalt `user:$$apr1$$6I07kKG9$$Ft65MGOP7mFnWMqnphlXW/` muss auf meine eigenen Bedürfnisse angepasst werden! Das Wunschpasswort lässt sich gannz einfach mit `echo $(htpasswd -nb <username> <password>)` generieren

#### 3. docker-compose.yml

Die wichtigste Datei. Hier passieren die guten Dinge. Das Schöne an Traefik ist, dass es sehr einfach ist, neue Container bereitzustellen, sobald ich die ersten Einstellungen vorgenommen habe. Dies funktioniert durch die Angabe von Labels für meine Container.

```
# docker-compose.yml
version: '3.5'

services:
  traefik:
    image: traefik:latest
    container_name: Traefik
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    networks:
      - proxy
    ports:
      - 80:80
      - 443:443
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./traefik-data/traefik.yml:/traefik.yml:ro
      - ./traefik-data/acme.json:/acme.json
      - ./traefik-data/configurations:/configurations
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.traefik-secure.entrypoints=websecure"
      - "traefik.http.routers.traefik-secure.rule=Host(`traefik.meinedomain.com`)"
      - "traefik.http.routers.traefik-secure.middlewares=user-auth@file"
      - "traefik.http.routers.traefik-secure.service=api@internal"
      # Folgende Konfigurationen nicht notwendig da bereits in dynamic.yml / traefik.yml definiert
      # - "traefik.http.middlewares.traefik-auth.basicauth.users=xxxxx:xxxxxxxxx"
      # - "traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https"
      # - "providers.file.filename=/dynamic_conf.yml"
      # - "traefik.http.routers.traefik-secure.middlewares=secHeaders@file,traefik-auth"
      # - "traefik.http.routers.traefik.middlewares=traefik-https-redirect"

  portainer:
    image: portainer/portainer-ce:latest
    container_name: Portainer
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    networks:
      - proxy
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./portainer-data:/data
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.portainer-secure.entrypoints=websecure"
      - "traefik.http.routers.portainer-secure.rule=Host(`portainer.meinedomain.com`)"
      - "traefik.http.routers.portainer-secure.service=portainer"
      - "traefik.http.services.portainer.loadbalancer.server.port=9000"

networks:
  proxy:
    external: true
```
Beispiel-Inhalt `meinedomain.com` muss auf meine eigenen Bedürfnisse angepasst werden!

Für jeden Container, den Traefik bearbeiten soll, fügen ich Labels hinzu, damit Traefik weiß, wohin es ihn weiterleiten soll. Wenn ich mir also die obige Datei ansehen, sollte ich schnell überprüfen, was im Traefik-Container passiert.

Ich füge also das erste Label `enable=true` hinzu, das Traefik mitteilt, dass es diesen Container weiterleiten soll.
Dies ist notwendig, da ich in der Konfiguration (traefik.yml) explizit `exposedByDefault: false` angegeben habe, so dass ich es immer definieren muss.

Das zweite Label sagt, dass ich das Netzwerk "proxy" verwenden möchte, welches ich im folgenden noch erstellen werden. Danach teile ich Traefik mit, dass es meinen Endpunkt `websecure` (https) verwenden soll. Dann gebe ich meinen Hostnamen mit der entsprechenden Domain an. 👍

Das vorletzte Label spezifiziert den API-Handler. Er stellt Informationen wie die Konfiguration aller Router, Dienste, Middlewares usw. zur Verfügung. Alle verfügbaren Endpunkte kann ich in der [Dokumentation von Traefik v2.5](https://doc.traefik.io/traefik/v2.5/operations/api/#endpoints) einsehen.

Das letztes Label ist meine grundlegende Auth-Middleware. Da das Traefik-Dashboard standardmäßig öffentlich zugänglich ist, füge ich einen Passwortschutz hinzu. Sie schützt auch meine API.

```
labels:
  - "traefik.enable=true"
  - "traefik.docker.network=proxy"
  - "traefik.http.routers.traefik-secure.entrypoints=websecure"
  - "traefik.http.routers.traefik-secure.rule=Host(`traefik.meinedomain.com`)"
  - "traefik.http.routers.traefik-secure.service=api@internal"
  - "traefik.http.routers.traefik-secure.middlewares=user-auth@file"
```

### Stack ausführen 🚀

#### 1. Netzwerk "proxy" anlegen

Ich muss ein neues Docker-Netzwerk erstellen, das Datenverkehr von außen zulässt. Dieses sollte "proxy" heißen, wie ich es in meiner `docker-compose.yml` angegeben habe:

```
networks:
  - proxy
```

Um dieses Netzwerk zu erstellen verwende ich folgenden Befehl:

```
$ docker network create proxy
```

#### 2. Setzen der richtigen Berechtigungen für acme.json

Standardmäßig ist die Berechtigung für die Datei acme.json auf 644 gesetzt, was zu einem Fehler beim Ausführen von `docker-compose` führen wird. Also stelle ich sicher, dass die Berechtigungen für diese Datei auf 600 gesetzt ist. Dafür wechsele ich mit `cd` in den Ordner `core` und führe den folgenden Befehl aus:
```
$ sudo chmod 600 ./traefik-data/acme.json
```

#### 3. Stack starten

Nun ist es an der Zeit, den Stack zu starten. Ich stelle sicher, dass ich mich im Ordner `core` befinden, damit Docker die Datei `docker-compose.yml` finden kann. Beim ersten Durchlauf überprüfe ich den Prozess auf Fehler, bevor ich das Flag `docker-compose --detach` verwende. Ich führe folgenden Befehl aus:

```
$ docker-compose up
```

Im Moment sollte das Traefik-Dashboard unter traefik.meinedomain.com und portainer.meinedomain.com verfügbar sein! 🔥
Wenn ich sicher bin, dass meine Container korrekt laufen, kann ich sie im Hintergrund laufen lassen, indem ich die Option `--detach` verwende:
```
$ docker-compose down && docker-compose up -d
```

### Quellen

- https://goneuland.de/traefik-v2-reverse-proxy-fuer-docker-unter-debian-10-einrichten/
- https://goneuland.de/traefik-v2-https-verschluesselung-sicherheit-verbessern/
- https://rafrasenberg.com/posts/docker-container-management-with-traefik-v2-and-portainer/