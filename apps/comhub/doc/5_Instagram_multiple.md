# Multiple Instagram-Instanzen in Matrix einbringen


## Die Datenbank

Auch hier setzte ich auf `postgres`-Datenbanken. Da ich multiple Instanzen einsetzten möchte benötige ich auch mehrere Datenbanken. In meinem Fall benötige ich zwei WhatsApp Instanzen. Um auf Nummer sicher zu gehen erstelle ich drei für mich!

> ⚠️ Bitte Kommentare beachten und ggf. anpassen!

```yaml
# docker-server/apps/comhub/docker-compose.yml
  instagram-db-1:
    image: postgres:alpine
    restart: unless-stopped
    hostname: instagram-db-1
    environment:
      # Eigenes Passwort setzten!
      - POSTGRES_PASSWORD=EigenesPasswort
      - POSTGRES_USER=instagram1
      - POSTGRES_DB=instagram1
      - POSTGRES_INITDB_ARGS=--encoding='UTF8' --lc-collate='C' --lc-ctype='C'
    networks:
      - internal_network
    volumes:
      - ./synapse/.psqlrc:/root/.psqlrc:ro
      - ./instagram/1/db/:/var/lib/postgresql/data/
  instagram-db-2:
    image: postgres:alpine
    restart: unless-stopped
    hostname: instagram-db-2
    environment:
      # Eigenes Passwort setzten!
      - POSTGRES_PASSWORD=EigenesPasswort
      - POSTGRES_USER=instagram2
      - POSTGRES_DB=instagram2
      - POSTGRES_INITDB_ARGS=--encoding='UTF8' --lc-collate='C' --lc-ctype='C'
    networks:
      - internal_network
    volumes:
      - ./synapse/.psqlrc:/root/.psqlrc:ro
      - ./instagram/2/db/:/var/lib/postgresql/data/
  instagram-db-3:
    image: postgres:alpine
    restart: unless-stopped
    hostname: instagram-db-3
    environment:
      # Eigenes Passwort setzten!
      - POSTGRES_PASSWORD=EigenesPasswort
      - POSTGRES_USER=instagram3
      - POSTGRES_DB=instagram3
      - POSTGRES_INITDB_ARGS=--encoding='UTF8' --lc-collate='C' --lc-ctype='C'
    networks:
      - internal_network
    volumes:
      - ./synapse/.psqlrc:/root/.psqlrc:ro
      - ./instagram/3/db/:/var/lib/postgresql/data/
```

## Konfiguration generieren

Nun kann ich die Konfigurationdateien von WhatsApp generieren lassen. Da ich multiple Instanzen verwenden möchte muss ich dieses auch mehrfach generieren. Ich führe folgenden Befehle im Verzeichnis `comhub` aus.

```shell
docker run --rm -v `pwd`/instagram/1/data:/data:z dock.mau.dev/mautrix/instagram:latest
docker run --rm -v `pwd`/instagram/2/data:/data:z dock.mau.dev/mautrix/instagram:latest
docker run --rm -v `pwd`/instagram/3/data:/data:z dock.mau.dev/mautrix/instagram:latest
```

## Konfigurationen anpassen

Nach dem die Konfiguraionen von mir nun erstellt wurden muss ich diese noch auf meine Bedürnisse anpassen!

> ⚠️ Jede config.yml muss angepasst werden
> `instagram/1/data/config.yml`
> `instagram/2/data/config.yml`
> `instagram/3/data/config.yml`
> 
> ⚠️ Bitte Kommentare beachten und ggf. anpassen!

```yaml
homeserver:
    # Anpassen auf eigene Domain
    address: https://matrix.meinedomain.com
    # Anpassen auf eigene Domain
    domain: matrix.meinedomain.com
    asmux: false
    status_endpoint: null
    message_send_checkpoint_endpoint: null

appservice:
    # Die Adresse muss für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = http://whatsapp-1:10001
    # WhatsApp-Instanz 2 = http://whatsapp-2:10002
    # usw.
    address: http://whatsapp-1:10001
    hostname: 0.0.0.0
    # Der Port muss für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = 10001
    # WhatsApp-Instanz 2 = 10002
    # usw.
    port: 10001
    
    database:
        type: postgres
        # Die Datenbank und das dazugehörige Passwort muss für jede Instanz angepasst werden
        # WhatsApp-Instanz 1 = postgresql://whatsapp1:EigenesPasswort@whatsapp-db-1:5432/whatsapp1?sslmode=disable
        # WhatsApp-Instanz 2 = postgresql://whatsapp2:EigenesPasswort@whatsapp-db-2:5432/whatsapp2?sslmode=disable
        # usw.
        uri: postgresql://whatsapp1:EigenesPasswort@whatsapp-db-1:5432/whatsapp1?sslmode=disable
        max_open_conns: 20
        max_idle_conns: 2
    provisioning:
        prefix: /_matrix/provision/v1
        shared_secret: generate
    
    # ⚠️  Die ID muss für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = whatsapp1
    # WhatsApp-Instanz 2 = whatsapp2
    # usw.
    id: whatsapp1
    bot:
        # ⚠️  Der Username muss für jede Instanz angepasst werden
        # WhatsApp-Instanz 1 = whatsappbot1
        # WhatsApp-Instanz 2 = whatsappbot2
        # usw.
        username: whatsappbot1
        # Der displayname sollte für jede Instanz angepasst werden
        # WhatsApp-Instanz 1 = WhatsApp Bridge 1
        # WhatsApp-Instanz 2 = WhatsApp Bridge 2
        # usw.
        displayname: WhatsApp Bridge 1
        avatar: mxc://maunium.net/NeXNQarUbrlYBiPCpprYsRqr
    ephemeral_events: false
    # Die fehlenden Schlüssel werden beim ersten Start generiert
    as_token: "This value is generated when generating the registration"
    hs_token: "This value is generated when generating the registration"
metrics:
    enabled: false
    listen: 127.0.0.1:8001
whatsapp:
    # Der os_name sollte für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = Mautrix-WhatsApp Bridge 1 on meinedomain.com
    # WhatsApp-Instanz 2 = Mautrix-WhatsApp Bridge 2 on meinedomain.com
    os_name: Mautrix-WhatsApp Bridge 1 on meinedomain.com
    browser_name: unknown

bridge:
    # ⚠️  Die username_template muss für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = whatsapp1_{{.}}
    # WhatsApp-Instanz 2 = whatsapp2_{{.}}
    # usw.
    username_template: whatsapp1_{{.}}
    # ⚠️  Die displayname_template muss für jede Instanz angepasst werden
    # WhatsApp-Instanz 1 = {{if .PushName}}{{.PushName}}{{else if .BusinessName}}{{.BusinessName}}{{else}}{{.JID}}{{end}} (WA1)
    # WhatsApp-Instanz 2 = {{if .PushName}}{{.PushName}}{{else if .BusinessName}}{{.BusinessName}}{{else}}{{.JID}}{{end}} (WA2)
    # usw.
    displayname_template: "{{if .PushName}}{{.PushName}}{{else if .BusinessName}}{{.BusinessName}}{{else}}{{.JID}}{{end}} (WA1)"
    delivery_receipts: false
    call_start_notices: true
    identity_change_notices: false
    reaction_notices: true
    portal_message_buffer: 128
    history_sync:
        create_portals: true
        max_age: 604800
        backfill: false
        double_puppet_backfill: false
        request_full_sync: false
    user_avatar_sync: true
    bridge_matrix_leave: true
    sync_with_custom_puppets: true
    sync_direct_chat_list: false
    default_bridge_receipts: false
    default_bridge_presence: false
    double_puppet_server_map:
        # Sollte angepasst werden.
        matrix.meinedomain.com: https://matrix.meinedomain.com
    double_puppet_allow_discovery: false
    login_shared_secret_map:
        example.com: foobar
    private_chat_portal_meta: true
    bridge_notices: true
    resend_bridge_info: false
    mute_bridging: false
    archive_tag: null
    pinned_tag: null
    tag_only_on_create: true
    enable_status_broadcast: true
    mute_status_broadcast: true
    whatsapp_thumbnail: false
    allow_user_invite: false
    federate_rooms: true
    command_prefix: "!wa1"
    management_room_text:
        # Die welcome sollte für jede Instanz angepasst werden
        # WhatsApp-Instanz 1 = Hallo, Ich bin der WhatsApp Bridge Bot 1.
        # WhatsApp-Instanz 2 = Hallo, Ich bin der WhatsApp Bridge Bot 2.
        # usw.
        welcome: "Hallo, Ich bin der WhatsApp Bridge Bot 1."
        welcome_connected: "Benutze `help` für Hilfe"
        welcome_unconnected: "Benutze `help` für Hilfe oder `login` zum Login"
        additional_help: ""
    encryption:
        allow: true
        default: true
        key_sharing:
            allow: true
            require_cross_signing: false
            require_verification: false
    permissions:
        # Die Domain und der Adminnutzer müssen gesetzt werden
        "*": relay
        "matrix.meinedomain.com": user
        "@admin:matrix.meinedomain.com": admin
    
    relay:
        enabled: false
        admin_only: true
        message_formats:
            m.text: "<b>{{ .Sender.Displayname }}</b>: {{ .Message }}"
            m.notice: "<b>{{ .Sender.Displayname }}</b>: {{ .Message }}"
            m.emote: "* <b>{{ .Sender.Displayname }}</b> {{ .Message }}"
            m.file: "<b>{{ .Sender.Displayname }}</b> sent a file"
            m.image: "<b>{{ .Sender.Displayname }}</b> sent an image"
            m.audio: "<b>{{ .Sender.Displayname }}</b> sent an audio file"
            m.video: "<b>{{ .Sender.Displayname }}</b> sent a video"
            m.location: "<b>{{ .Sender.Displayname }}</b> sent a location"
logging:
    directory: ./logs
    file_name_format: "{{.Date}}-{{.Index}}.log"
    file_date_format: "2006-01-02"
    file_mode: 0o600
    timestamp_format: "Jan _2, 2006 15:04:05"
    print_level: debug

```

Es ist möglich, dass ich den angepassten Inhalt hier in die jeweilige `config.yaml` kopiere.

## Die `registration.yaml` generieren

Nach dem ich die jeweilige `config.yaml` angepasst habe kann ich die dazugehörigen `registration.yaml` generieren. Ich führe folgenden Befehle im Verzeichnis `comhub` aus.

```shell
docker run --rm -v `pwd`/instagram/1/data:/data:z dock.mau.dev/mautrix/instagram:latest
docker run --rm -v `pwd`/instagram/2/data:/data:z dock.mau.dev/mautrix/instagram:latest
docker run --rm -v `pwd`/instagram/3/data:/data:z dock.mau.dev/mautrix/instagram:latest
```

## WhatsApp-Instanzen in Docker-Compose integrieren

```yaml
  # docker-server/apps/comhub/docker-compose.yml
  instagram-1:
    image: dock.mau.dev/mautrix/instagram:latest
    restart: unless-stopped
    environment:
      - UID=1000
      - GID=1000
    networks:
      - internal_network
    volumes:
    - ./instagram/1/data/:/data
    expose:
      - "20001"
    depends_on:
      - instagram-db-1
      - synapse
  instagram-2:
    image: dock.mau.dev/mautrix/instagram:latest
    restart: unless-stopped
    environment:
      - UID=1000
      - GID=1000
    networks:
      - internal_network
    volumes:
    - ./instagram/2/data/:/data
    expose:
      - "20002"
    depends_on:
      - instagram-db-2
      - synapse
  instagram-3:
    image: dock.mau.dev/mautrix/instagram:latest
    restart: unless-stopped
    environment:
      - UID=1000
      - GID=1000
    networks:
      - internal_network
    volumes:
    - ./instagram/3/data/:/data
    expose:
      - "20003"
    depends_on:
      - instagram-db-3
      - synapse
```

## Weitere Anpassungen an der `docker-compose.yml`

Ich muss nun noch ein paar Änderungen an der `docker-compose.yml` vornehmen. Ich möchte, dass der Service `synapse` auf die `registration.yaml` zugriff bekommt. Das ist auch der Grund warum ich jeder WhatsApp Instanz das `environment` mitgegeben habe.

```yaml
  # Bereits vorhanden Eintrag der docker-compse.yml bearbeiten
  synapse:
    image: matrixdotorg/synapse:latest
    restart: unless-stopped
    volumes:
      - ./synapse/data/:/data
      - ./whatsapp:/whatsapp
      - ./instagram:/instagram
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.synapse.rule=Host(`synapse.meinedomain.com`)"
      - "traefik.http.routers.synapse.entrypoints=websecure"
      - "traefik.http.services.synapse.loadbalancer.server.port=8008"
    environment:
      - SYNAPSE_CONFIG_DIR=/data
      - SYNAPSE_CONFIG_PATH=/data/homeserver.yaml
      - UID=1000
      - GID=1000
      - TZ=Europe/Paris
    networks:
      - internal_network
      - proxy
    depends_on:
      - synapse-db
      - synapse-redis
```


## WhatsApp Instanzen nun noch mit Matrix verbinden

Als letztes muss ich noch die `homeserver.yaml` von Synapse anpassen. Im ersten Teil habe ich den Teil bereits ausgeklammert und kann diesen nun aktivieren:

```yaml
# Wird erst später benötigt. Deswegen erstmal auskommentiert!
#app_service_config_files:
#  - /mautrix-whatsapp/registration.yaml
#  - /mautrix-signal/registration.yaml
#  - /matrix-appservice-irc/appservice-registration-irc.yaml
```
Das ändere ich zu folgendem:

```yaml
app_service_config_files:
  - /instagram/1/data/registration.yaml
  - /instagram/2/data/registration.yaml
  - /instagram/3/data/registration.yaml
```

## Stack starten

Nun heißt es den Stack noch zu starten. Da ich an der `homeserver.yaml` etwas geändert habe und auch an teilen der `docker-compose.yml` lohnt es sich das gesamte Stack nochmal zu starten

```shell
docker-compose down && docker-compose up
```

Ich starte es erstmal ohne `--detach` um mögliche Fehler zu sehen. Ein gerne auftretender Fehler ist das auftretten von Probleme mit den Rechten auf die  `registration.yaml` zuzugreifen.
Wenn alles läuft und ich es im besten Fall auch getestet habe:

```shell
docker-compose down && docker-compose up
```