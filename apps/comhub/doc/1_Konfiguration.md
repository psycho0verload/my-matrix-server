# Meine Matrix Konfiguration

## Warum sollte man selbst hosten?

## Enter the matrix

Ich finde es übersichtlich, alle meine `compose`-Dateien in separaten Verzeichnissen aufzubewahren, dies möchte ich hier auch machen. Zielverzeichnis für meinen comhub wird `~/docker-server/apps/comhub` in diesem Projekt.

### Das Wichtigste zuerst: die Matrix-Datenbank

Als erstes definiere ich die Datenbank für den Matrix-Server. Als Datennak lese ich überall, dass bevorzugt Postgres genutzt wird. Also möchte ich das auch so nutzen.

> ⚠️ Beispiel-Inhalt `POSTGRES_PASSWORD=EigenesPasswortSetzen`, `POSTGRES_USER=synapse_user` und `POSTGRES_DB=synapse` müssen auf meine eigenen Bedürfnisse angepasst werden!
```yaml
#docker-compose.yml
version: '3.7'
services:
  synapse-db:
    image: postgres:latest
    restart: unless-stopped
    hostname: synapse-db
    environment:
      - POSTGRES_PASSWORD=EigenesPasswortSetzen
      - POSTGRES_USER=synapse_user
      - POSTGRES_DB=synapse
      - POSTGRES_INITDB_ARGS=--encoding='UTF8' --lc-collate='C' --lc-ctype='C'
    volumes:
      - ./.psqlrc:/root/.psqlrc:ro
      - ./synapse/db/:/var/lib/postgresql/data/
    networks:
      - internal_network
#
# Hier kommen im folgenden noch mehr Konfigurationen 
#

# Am Ende der Datei muss folgendes noch definiert werden
networks:
  proxy:
    external: true
  internal_network:
    external: true
  external_network:
    external: true
```
Um bei Bedarf Postgres konfigurieren zu können setzte ich den Pfad für `.psqlrc`. Die Datei muss ich generieren bevor ich den Container das erste mal starte. Dazu gibt es einen interessanten [Artikel](https://tapoueh.org/blog/2017/12/setting-up-psql-the-postgresql-cli/).

```
#synapse/.psqlrc
\set PROMPT1 '%~%#%x '
\x auto
\set ON_ERROR_STOP on
\set ON_ERROR_ROLLBACK interactive
\set HISTFILE ~/.psql_history-:DBNAME

\set VERBOSITY verbose

-- \pset null '⦱'
\pset null '¤'
\pset linestyle 'unicode'

\pset unicode_border_linestyle single
\pset unicode_column_linestyle single
\pset unicode_header_linestyle double

set intervalstyle to 'postgres_verbose';

\setenv LESS '-iMFXSx4R'
```

### Synapse einrichten und mein Gehirn mit der Matrix verbinden 😅

#### DNS Records setzen

An dieser Stelle benötige ich die ersten beiden Domains für Matrix. Ich richte sie so ein, dass sie auf meinen Server zeigen:

> ⚠️ Beispiel-Inhalt `*.meinedomain.com` und `123.321.216.34` müssen auf meine eigenen Bedürfnisse angepasst werden!
```
synapse.meinedomain.com. IN A 123.321.216.34 
matrix.meinedomain.com. IN A 123.321.216.34
_matrix-identity._tcp SRV record mit 10 0 443 matrix.meinedomain.com
```

#### Problematik mit statischen Dateien

Traefik kann keine statischen Dateien bereitstellen, daher kann ich Traefik nur als Reverse-Proxy für den Synapse-Container verwenden, und für die statische Dateien benutze ich nignx, die für den Betrieb des Verbunds benötigt werden ([siehe die Dokumentation hier](https://github.com/matrix-org/synapse/blob/master/docs/federate.md)).

Der einzige Grund, warum es nginx im Stack gibt, ist, dass traefik nur ein Reverse Proxy ist und keine statischen Dateien bereitstellen kann, die wir für die Föderation im Verzeichnis `.well-know/` benötigen.

#### Synapse Konfiguration generieren

In der Dokumentation steht nun, dass man den synapse-Container einmal starten soll, um eine Konfiguration zu erhalten, diese zu bearbeiten und dann erneut zu starten - ein Prozess, den man bei so ziemlich häufig bei Containern sieht.

Ich wechsel mit `cd` in den Ordner `synapse/data` und lasse dort die Konfiguration generieren.

> ⚠️ Beispiel-Inhalt `matrix.meinedomain.com` muss die Domain beinhalten, die ich bei "[DNS Records setzen](#-DNS-Records-setzen)" festgelegt habe!
```shell
docker run -it --rm \
    -v $(pwd):/data \
    -e SYNAPSE_SERVER_NAME=matrix.meinedomain.com \
    -e SYNAPSE_REPORT_STATS=yes \
    -e UID=1000 \
    -e GID=1000 \
    matrixdotorg/synapse:latest generate
```

Ich erhalte die Datei `homeserver.yaml`, die ich noch bearbeitet muss. Beispiel ohne Kommentare (Bis auf die Hinweise wo etwas bearbeitet werden muss).
> ⚠️ Die Beispiel `homeserver.yaml`kann einfach anstelle von der generierten verwendert werden. Dafür muss zuerst diese Beispieldatei angepasst werden und teilweise Informationen aus der generierten Datei übernommen werden!

```yaml
# Domain "meinedomain.com" anpassen
server_name: "meinedomain.com"
pid_file: /data/homeserver.pid
# Domain https://element.meinedomain.com/ anpassen
web_client_location: https://element.meinedomain.com/
# Domain https://synapse.meinedomain.com/ anpassen
public_baseurl: https://synapse.meinedomain.com/
listeners:
  - port: 8008
    tls: false
    type: http
    x_forwarded: true
    resources:
      - names: [client, federation]
        compress: false
# Email-Adress anpassen
admin_contact: 'mailto:matrix@meinedomain.com'
limit_remote_rooms:
retention:
  enabled: true
caches:
   per_cache_factors:
database:
  name: psycopg2
  args:
    # Zuvor festgelegten Datenbank informationen hier einfügen!
    user: GibMirDenDefiniertenUser
    password: GibMirDasDefiniertePasswort
    database: GibMirDieDefinierteDatenbank
    host: synapse-db
    cp_min: 5
    cp_max: 10
# Pfad ""/data/meinedomain.com.log.config" anpassen
log_config: "/data/meinedomain.com.log.config"
media_store_path: "/data/media_store"
url_preview_accept_language:
  - en
  - de
enable_registration: true
account_validity:
# Das Geheimnis der von mir generierten Datei übernehmen
registration_shared_secret: "PasswortAusGeneriertenDateiÜbernehmen"
account_threepid_delegates:
metrics_flags:
report_stats: true
# Wird erst später benötigt. Deswegen erstmal auskommentiert!
#app_service_config_files:
#  - /mautrix-whatsapp/registration.yaml
#  - /mautrix-signal/registration.yaml
#  - /matrix-appservice-irc/appservice-registration-irc.yaml
# Das Geheimnis der von mir generierten Datei übernehmen
macaroon_secret_key: "PasswortAusGeneriertenDateiÜbernehmen"
# Das Geheimnis der von mir generierten Datei übernehmen
form_secret: "PasswortAusGeneriertenDateiÜbernehmen"
# Pfad "/data/matrix.meinedomain.com.signing.key" anspassen
signing_key_path: "/data/meinedomain.com.signing.key"
old_signing_keys:
trusted_key_servers:
  - server_name: "matrix.org"
saml2_config:
  sp_config:
  user_mapping_provider:
    config:
oidc_providers:
cas_config:
sso:
password_config:
   policy:
      enabled: true
      minimum_length: 16
      require_digit: true
ui_auth:
email:
  smtp_host: smtp
  smtp_port: 25
  #smtp_user: "matrix@meinedomain.com"
  #smtp_pass: "PasswordOfEmailAdress"
  require_transport_security: false
  notif_from: "Your Friendly %(app)s homeserver <matrix@meinedomain.com>"
  # App Namen definieren
  app_name: CHANGEME
  enable_notifs: true
password_providers:
push:
user_directory:
opentracing:
redis:
  enabled: true
  host: synapse-redis
  port: 6379
```
Natürlich kann ich hier noch mehr Dinge anpassen, aber für mich reicht das.

#### Synapse zu `docker-compose.yml` hinzufügen

Nun muss ich die `docker-compose.yml` um den Matrix-Server `synapse` und `redis` erweitern

> ⚠️ Beispiel-Inhalt `synapse.meinedomain.com` muss auf meine eigenen Bedürfnisse angepasst werden!
```yaml
#docker-compose.yml
# [...]
  synapse-redis:
    image: redis:6-alpine
    restart: unless-stopped
    hostname: synapse-redis
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
    volumes:
      - ./synapse/redis:/data
    networks:
      - internal_network
  synapse:
    image: matrixdotorg/synapse:latest
    restart: unless-stopped
    volumes:
      - ./synapse/data/:/data
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.synapse.rule=Host(`synapse.meinedomain.com`)"
      - "traefik.http.routers.synapse.entrypoints=websecure"
      - "traefik.http.services.synapse.loadbalancer.server.port=8008"
    environment:
      - SYNAPSE_CONFIG_DIR=/data
      - SYNAPSE_CONFIG_PATH=/data/homeserver.yaml
      - UID=1000
      - GID=1000
      - TZ=Europe/Paris
    networks:
      - internal_network
      - proxy
    depends_on:
      - synapse-db
      - synapse-redis
  smtp:
    image: namshi/smtp
    container_name: smtp
    restart: always
  networks:
      - internal_network
```
Jetzt sollten ich in der Lage sein, auf https://synapse.meinedomain.com etwas zu sehen.
Herzlichen Glückwunsch, das ist ein Schritt näher zum vollständigen Betrieb von Matrix mit der Föderation!

#### Verbund komplettieren 🤩

Das ist der Moment an dem ich nun `nginx` in den Stack mit aufnehme.

> ⚠️ Beispiel-Inhalt `matrix.meinedomain.com` muss die Domain beinhalten, die ich bei "[DNS Records setzen](#-DNS-Records-setzen)" festgelegt habe!

```yaml
  nginx:
    image: nginx:mainline-alpine
    restart: unless-stopped
    volumes:
      - ./nginx/matrix.conf:/etc/nginx/conf.d/matrix.conf
      - ./nginx/www:/var/www/
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.matrix.rule=Host(`matrix.meinedomain.com`)"
      - "traefik.http.routers.matrix.entrypoints=websecure"
      - "traefik.http.services.matrix.loadbalancer.server.port=80"
    networks:
      - internal_network
      - proxy
```

Nichts wirklich Ausgefallenes, ich muss die statischen Dateien einrichten, die im `/var/www/` innerhalb des Containers bereitgestellt werden.
Die Konfiguration besteht aus zwei Speicherorten, von denen einer als Reverse-Proxy für den Synapse-Container dient und der andere die statischen Dateien bereitstellt.

Innerhalb des `comhub`kann ich nun mit `mkdir nginx && cd nginx && nano matrix.conf`

> ⚠️ Beispiel-Inhalt `matrix.meinedomain.com` muss die Domain beinhalten, die ich bei "[DNS Records setzen](#-DNS-Records-setzen)" festgelegt habe!

```config
# nginx/matrix.conf
server {
  listen         80 default_server;
  server_name    matrix.meinedomain.com;

 location /_matrix {
    proxy_pass http://synapse:8008;
    proxy_set_header X-Forwarded-For $remote_addr;
    client_max_body_size 128m;
  }

  location /.well-known/matrix/ {
    root /var/www/;
    default_type application/json;
    add_header Access-Control-Allow-Origin  *;
  }
}
```

Nun erstelle ich die Verbunddateien innerhalb des nginx-Containers mit:

```shell
mkdir -p www/.well-known/matrix/
nano www/.well-known/matrix/client
```

> ⚠️ Beispiel-Inhalt `matrix.meinedomain.com` muss die Domain beinhalten, die ich bei "[DNS Records setzen](#-DNS-Records-setzen)" festgelegt habe!
```json
{
  "m.homeserver": {
    "base_url": "https://matrix.meinedomain.com"
  }
}
```

`nano www/.well-known/matrix/server`

> ⚠️ Beispiel-Inhalt `synapse.meinedomain.com` muss die Domain beinhalten, die ich bei "[DNS Records setzen](#-DNS-Records-setzen)" festgelegt habe!
```json
{
  "m.server": "synapse.meinedomain.com:443"
}
```
Nun kann ich den nginx-Container ausführen. Dann ist es möglich das ganze zu testen auf https://federationtester.matrix.org/#matrix.meinedomain.com. Wenn alles gut gelaufen ist, sollten alle Kontrollen grün sein.

## Let's Elements

So, jetzt ist der Server installiert, Sie brauchen einen Client. Es gibt viele Möglichkeiten, ich werde Element verwenden. 
Das Einrichten ist einfach, ich erstelle  einen Riot-Ordner `mkdir element && cd element` dann hole ich mir eine Beispielkonfiguration mit `wget https://raw.githubusercontent.com/vector-im/element-web/develop/config.sample.json && mv config.sample.json config.json`.

Die Erweiterung von docker-compose ist auch nicht sonderlich groß:

```yaml
  element:
    image: vectorim/element-web:latest
    restart: unless-stopped
    volumes:
      - ./element/config.json:/app/config.json:ro
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.element.rule=Host(`element.meinedomain.com`)"
      - "traefik.http.routers.element.entrypoints=websecure"
      - "traefik.http.services.element.loadbalancer.server.port=80"
    networks:
      - internal_network
      - proxy
```

Jetzt sollte unter `element.meinedomain.com` mein Server erreichbar sein.

## Admin anlegen

Ich kann jetzt schon unter `element.meinedomain.com` einen Benutzer anlegen, allerdings hat dieser dann keine "admin"-Rechte. Um den ersten Nutzer anzulegen und diesem Rechte zuzuordnen muss ich einen kleinen Umweg gehen:

```
docker exec -it comhub_synapse_1 sh
register_new_matrix_user -c /data/homeserver.yaml https://synapse.meinedomain.com
```

## Admin-GUI

Zu guter letzt in diesem Teil möchte ich noch eine Admin-GUI einbringen. Dafür muss ich die docker-compse noch ein weiteres mal erweitern:

```yaml
  synapse-admin:
    image: "awesometechnologies/synapse-admin:latest"
    environment:
      REACT_APP_SERVER: "https://matrix.meinedomain.com"
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=proxy"
      - "traefik.http.routers.matrix-admin.entrypoints=websecure"
      - "traefik.http.routers.matrix-admin.rule=(Host(`admin.matrix.meinedomain.com`))"
      - "traefik.http.routers.matrix-admin.service=matrix-admin"
      - "traefik.http.services.matrix-admin.loadbalancer.server.port=80"
    restart: unless-stopped
    depends_on:
      - synapse
    networks:
      - internal_network
      - proxy
```

## Quellen
- https://www.brunweb.de/eigener-matrix-homeserver-mit-docker-einrichten/
- https://blog.bartab.fr/install-a-self-hosted-matrix-server-part-1/
- https://blog.bartab.fr/install-a-self-hosted-matrix-server-part-2/
- https://www.youtube.com/watch?v=aKdj56VVRqY
- https://zerowidthjoiner.net/2020/03/20/setting-up-matrix-and-riot-with-docker
- https://matrix.org/blog/2020/04/06/running-your-own-secure-communication-service-with-matrix-and-jitsi